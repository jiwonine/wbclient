#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <errno.h>
#include <openssl/crypto.h>
#include <openssl/aes.h>

#define uTrue	1
#define uFalse	0
#define MAX_USER_BUF	16
#define	MAX_PASS_BUF	16
#define MAX_ADDR_BUF	50
#define MAX_STR_BUF	256
#define MAX_PATH_BUF	1024
#define MAX_CMD_BUF	4096
#define BLOCK_SIZE	16
#define	FREAD_COUNT	4096
#define	RW_SIZE		1


#define ERR_CLIENT_CMD_PARAM_INVAILD	999
#define ERR_CLIENT_ENV_HOME_INVALID	998
#define ERR_CLIENT_CONFIG_ROAD		997
#define ERR_DUMP_WALLET			996
#define ERR_ENCRYPT_WALLET		995
#define ERR_RSYNC_CMD			994
#define ERR_DECRYPT_WALLET		993
#define ERR_WALLET_PATH_INVALID		992
#define ERR_PUBKEY_INCORRECT		991

#define WBC_FAIL_MSG(cmd) \
       wbc_errmsg_print(cmd)


#define DAEMON_BTC	1
#define DAEMON_ETH	2
#define DAEMON_QTUM	3
#define DAEMON_SIA	4
#define DAEMON_RIPLE	5
#define DAEMON_ION	6

typedef struct _SConfig {
int  dCategory;
char backupDir[MAX_PATH_BUF];
char remoteIp[MAX_ADDR_BUF];
char remoteSvc[MAX_STR_BUF];
char remoteUser[MAX_USER_BUF];
char monPath[MAX_STR_BUF];
int  remotePort;
int  period;
} wbcConfig;


void wbc_timer_event(int signo);
void wbc_errmsg_print(int cmd);
void wbc_strcut_endspace(char *src);
int wbc_exe_isvalid(int argc, char** argv);
int wbc_parser(char* allData, char* key, char* del, char* value);
int wbc_road_config(char* filePath, wbcConfig* resCfg);
int wbc_dump_wallet(wbcConfig* cfg, char* outPath, char* outaesPath);

int wbc_file_encrypt_aes(char* inFile, char* outFile);
int wbc_file_decrypt_aes(char* inFile, char* outFile);
int wbc_wallet_encrypt(char* filePath, char* outPath);
int wbc_send_backupSv(char* aesFilePath);


