#include "WBCMain.h"

AES_KEY		gKey;
wbcConfig	gConfig;
int		gTimePeriod;
char		gPubKey[MAX_PASS_BUF];

static const unsigned char testkey[16]={"pandamama"};
unsigned char iv[16];

int wbc_parser(char* allData, char* key, char* del, char* value)
{
	/* allData = 기본 스트링
	 * (ex) 'mysqlip:127.0.0.1' 중
	 * key = mysqlip
	 * del = ':' 구분자
	 * value = 127.0.0.1
	 */
	if(*allData == '\0') {
		return uFalse;
	}
	char	*nChkBuf;
	char	nKeyBuf[MAX_STR_BUF];
	char	nValBuf[MAX_STR_BUF];
	memset(nKeyBuf, 0x00, MAX_STR_BUF);
	memset(nValBuf, 0x00, MAX_STR_BUF);
	int	nKeyLen=0, nValLen=0;

	if(*del == '\0') {
		nChkBuf = strchr((char*)allData, '=')+1;
	}else {
		nChkBuf = strchr((char*)allData, *del)+1;
	}

	if(strncmp(allData, key, strlen(key)) == 0) {
		nKeyLen = nChkBuf - (char*)allData -1;
		nValLen = strlen(allData) - nKeyLen -1;

		// KEY GET
		memcpy(nKeyBuf, allData, nKeyLen);
		nKeyBuf[nKeyLen] = 0;

		// VALUE GET
		memcpy(nValBuf, allData+nKeyLen+RW_SIZE, nValLen+RW_SIZE);
		nValBuf[nValLen-RW_SIZE] =0;
		strncpy(value, nValBuf, MAX_STR_BUF);
	}


	return uTrue;		
}


int wbc_road_config(char* filePath, wbcConfig* resCfg)
{
	char	nLine[FREAD_COUNT];
	char	nBuf[MAX_STR_BUF];
	char	nKeyBuf[MAX_STR_BUF];
	int	nValLen=0, nKeyLen=0, nRes=0;
	FILE 	*cfgInfo;

	cfgInfo	= fopen(filePath, "r");
	if(cfgInfo != NULL) {
		int nCnt=0;
		memset(resCfg, 0x00, sizeof(wbcConfig));
		while(fgets(nLine, sizeof(nLine), cfgInfo) != NULL) {
			// init	
			memset(nBuf, 0x00, MAX_STR_BUF);
			memset(nKeyBuf, 0x00, MAX_STR_BUF);
			nValLen=0;
			nKeyLen=0;
			nRes=0;
			const char* nSeq= strchr(nLine, '=');	
			if(nSeq == NULL)  {
				continue;
			}

			char *nChkBuf = strchr((char*)nLine, '=')+1;
			/* Category : BTC/ETH/QTUM/SIA/RIPLE/ION */
			if(strncmp(nLine, "Category", 8) == 0){
				nRes = wbc_parser(nLine, "Category", "=", nBuf);
				if(nRes == uFalse) {
					printf("Category Value Catch FAIL\n");
					return uFalse;
				}
				resCfg->dCategory = atoi(nBuf);
			}
			/* backupDir */
			if(strncmp(nLine, "BackupDir", 9) == 0) {
				nRes = wbc_parser(nLine, "BackupDir", "=", nBuf);
				if(nRes == uFalse) {
					printf("BackupDir Value Catch FAIL\n");
					return uFalse;
				}
				snprintf(resCfg->backupDir, MAX_PATH_BUF, "%s", nBuf);
			}
			/* remoteIp */
			if(strncmp(nLine, "RemoteIp", 8) == 0){
				nRes = wbc_parser(nLine, "RemoteIp", "=", nBuf);
				if(nRes == uFalse) {
					printf("RemoteIP value Catch FAIL\n");
					return uFalse;
				}	
				snprintf(resCfg->remoteIp, MAX_ADDR_BUF,  "%s", nBuf);
			}
			/* remoteSvc */
			if(strncmp(nLine, "RemoteSvc", 9) == 0){
				nRes = wbc_parser(nLine, "RemoteSvc", "=", nBuf);
				if(nRes == uFalse) {
					printf("RemoteSvc value Catch FAIL\n");
					return uFalse;
				}
				snprintf(resCfg->remoteSvc, MAX_STR_BUF, "%s", nBuf);
			}
			/* remoteUser */
			if(strncmp(nLine, "RemoteUser", 9) == 0) {
				nRes = wbc_parser(nLine, "RemoteUser", "=", nBuf);
				if(nRes == uFalse) {
					printf("RemotUser value Catch FAIL\n");
					return uFalse;
				}
				snprintf(resCfg->remoteUser, MAX_USER_BUF, "%s", nBuf);
			}
			/* remotePort */
			if(strncmp(nLine, "RemotePort", 10) == 0){
				nRes = wbc_parser(nLine, "RemotePort", "=", nBuf);
				if(nRes == uFalse) {
					printf("RemotePort value Catch FAIL\n");
					return uFalse;
				}
				resCfg->remotePort = atoi(nBuf); 
			}
			/* remotePeriod */
			if(strncmp(nLine, "RemotePeriod", 12) == 0){
				nRes = wbc_parser(nLine, "RemotePeriod", "=", nBuf);
				if(nRes == uFalse) {
					printf("RemotePeriod value Catch FAIL, default setting 1Hour\n");
					resCfg->period = 360; // default
				}
				resCfg->period = atoi(nBuf);
			}
			/* ETH/ERC20, SIA case */
			if(strncmp(nLine, "MonPath", 7) == 0){
				nRes = wbc_parser(nLine, "MonPath", "=", nBuf);
				if(nRes == uFalse) {
					printf("MonPath....Pass\n");
				}
				snprintf(resCfg->monPath, MAX_STR_BUF, "%s", nBuf);
			}
			nCnt++;
		}		

	}else {
		printf("WBClient.config Road Fail\n");
		return uFalse;
	}
	fclose(cfgInfo);
	return uTrue;
}

void wbc_strcut_endspace(char *src)
{
	int nLen =strlen(src);
	printf("nLen=%d\n\n\n", nLen);
	while(--nLen >> 0) {
		if (src[nLen] == ' ') {
			src[nLen] = 0x00;
			src[nLen] ='\0';
		}else {
			break;
		}
	}

	return;
}

int wbc_dump_wallet(wbcConfig* cfg, char* outPath, char* outaesPath)
{ 
	int	nRes = uFalse;
	char 	nCurCmd[MAX_CMD_BUF];
	char	nDCategory[MAX_USER_BUF];
	char	nDate[MAX_STR_BUF];
	
	memset(nDate, 0x00, MAX_STR_BUF);
	memset(nCurCmd, 0x00, MAX_CMD_BUF);
	memset(nDCategory, 0x00, MAX_USER_BUF);
	
	time_t	nTime;
	struct tm* nTimeInfo;

	time(&nTime);
	nTimeInfo= localtime(&nTime);
	snprintf(nDate, MAX_STR_BUF, "%04d%02d%02d_%02d%02d%02d",
			nTimeInfo->tm_year + 1900,
			nTimeInfo->tm_mon + 1,
			nTimeInfo->tm_mday,
			nTimeInfo->tm_hour,
			nTimeInfo->tm_min,
			nTimeInfo->tm_sec);

	/*- backupDir check -*/
	if(mkdir(cfg->backupDir, 0755) == -1 && errno != EEXIST) {
		printf("Directory Create Error:%s\n", strerror(errno));
		return uFalse;
	}

	/*- walletbackup all data delete --*/		
	snprintf(nCurCmd, MAX_CMD_BUF, "rm -rf %s/*", cfg->backupDir);
	printf("CurCmd:%s\n", nCurCmd);
	nRes = system(nCurCmd);

	
	snprintf(outPath, MAX_PATH_BUF, "%s/%s.dat", cfg->backupDir, nDate);	
	snprintf(outaesPath, MAX_PATH_BUF, "%s/%s_aes.dat", cfg->backupDir, nDate); 

	memset(nCurCmd, 0x00, MAX_CMD_BUF);
	
	switch(cfg->dCategory){
		case DAEMON_BTC:
			snprintf(nDCategory, MAX_USER_BUF, "dumpBTC");
			break;
		case DAEMON_ETH:
			snprintf(nDCategory, MAX_USER_BUF, "dumpETH");
			break;
		case DAEMON_QTUM:
			snprintf(nDCategory, MAX_USER_BUF, "dumpQTUM");
			break;
		case DAEMON_ION:
			snprintf(nDCategory, MAX_USER_BUF, "dumpION");
			break; // 2018.07.27 ION Add
		case DAEMON_SIA:
			snprintf(nDCategory, MAX_USER_BUF, "dumpSIA");
			break;
		case DAEMON_RIPLE:
			snprintf(nDCategory, MAX_USER_BUF, "dumpRIP");
			break;
		deafult:
			printf("Config Category is invalid, FAIL\n");
			return uFalse;
			break;
	}
	
	/*--*
	 * ubuntu 16.04 /bin/bash 실행 시 멈춤현상있음
	 * Process 에서 Command 명령으로 File 받는 부분에서 sh -c 멈춤.
	 * ETH/ERC20 은 바로 popen 사용하여 Command 처리
	 *--*/
	 // 2018.7.27 동일한 user 로 daemon + shell 돌리면 됨
	if(cfg->dCategory == DAEMON_ETH) {
		if(gConfig.monPath[0] == 0x00) {
			printf("Current Eth/ERC20 Case, configSetting(monPath)\n");
			return uFalse;
		}

		memset(nCurCmd, 0x00, MAX_CMD_BUF);
		snprintf(nCurCmd, MAX_CMD_BUF, "%s account list >> %s",  gConfig.monPath, outPath);
		printf("CurCmd 2nd:[%s]\n", nCurCmd);
		
		FILE* nWStream;
		nWStream = popen(nCurCmd, "w");
		if(nWStream == NULL) {
			printf("CurCmd Error:[%d:%s]\n", errno, strerror(errno));
			return uFalse;		
		}
		pclose(nWStream);
		printf("CurCmd 2nd Succ!\n");
		return uTrue;		
	}else if (cfg->dCategory == DAEMON_SIA) {
		if(gConfig.monPath[0] == 0x00) {
			printf("Current SIA Case, configSetting(monPath)\n");
			return uFalse;
		}
		
		memset(nCurCmd, 0x00, MAX_CMD_BUF);
		snprintf(nCurCmd, MAX_CMD_BUF, "%s wallet addresses >> %s", 
				gConfig.monPath, outPath);
		printf("CurCmd 2nd:[%s]\n", nCurCmd);
		
		FILE* nWStream;
		nWStream = popen(nCurCmd, "w");
		if(nWStream == NULL) {
			printf("CurCmd Error:[%d:%s]\n", errno, strerror(errno));
			return uFalse;
		}
		pclose(nWStream);
		printf("CurCmd 2nd Succ!\n");
		return uTrue;
		// printf("Current SIA Case, could you check script..?\n");

	}else {
		// BTC, QTUM, RIPLE, ION Case
	}

	/* (ex) $WBC_HOME/.WBCRun.sh dumpBTC 20180627121530.dat */
	snprintf(nCurCmd, MAX_CMD_BUF, "./WBCRun.sh %s %s", nDCategory, outPath);

	printf("CurCmd:%s\n", nCurCmd);
	
	FILE*   nStream;
	char    nBuf[MAX_STR_BUF];
	memset(nBuf, 0x00, MAX_STR_BUF);
	nStream = popen(nCurCmd, "r");
	if(nStream == NULL) {
		printf("CurCmd Error:[%d:%s]\n", errno, strerror(errno));
		return uFalse;
	}
	
	size_t nReadSize = fread((void*)nBuf, sizeof(char*), MAX_STR_BUF-1, nStream);
	if(nReadSize == 0) {
		pclose(nStream);
		printf("CurCmd Error:[%d:%s]\n", errno, strerror(errno));
		return uFalse;
	}
	
	pclose(nStream);
	if(strcmp(nBuf, "error") == 0) {
		printf("CurCmd Error:[%s]\n", nBuf);	
		return uFalse;
	}
	
	return uTrue;
}

int wbc_file_encrypt_aes(char* inFile, char* outFile) 
{
	int i=0;
	int nLen=0;
	int nPaddingLen=0;
	char nBuf[FREAD_COUNT+BLOCK_SIZE];
	
	FILE *nRFp=fopen(inFile,"rb");
	if( nRFp == NULL ){
		fprintf(stderr,"[ERROR] %d can not fopen('%s')\n",__LINE__, inFile);
		return uFalse;
	}

	FILE *nWFp=fopen(outFile,"wb");
	if( nWFp == NULL ){
		fprintf(stderr,"[ERROR] %d can not fopen('%s')\n",__LINE__, outFile);
		return uFalse;
	}

	memset(iv,0,sizeof(iv)); // init iv
	AES_set_encrypt_key(testkey ,256 ,&gKey);
	// Read File 
	while(nLen = fread(nBuf, RW_SIZE, FREAD_COUNT, nRFp) ){
		if(FREAD_COUNT != nLen ){
			break;
		}

		AES_cbc_encrypt(nBuf, nBuf, nLen, &gKey, iv, AES_ENCRYPT);
		fwrite(nBuf, RW_SIZE, nLen, nWFp);
	}

	nPaddingLen= BLOCK_SIZE - nLen % BLOCK_SIZE;
	//printf("ENC Padding Len:%d\n", nPaddingLen);

	memset(nBuf+nLen, nPaddingLen, nPaddingLen);
	AES_cbc_encrypt(nBuf, nBuf, nLen+nPaddingLen, &gKey, iv, AES_ENCRYPT);
	fwrite(nBuf, RW_SIZE, nLen+nPaddingLen, nWFp);

	fclose(nWFp);
	fclose(nRFp);

	return uTrue;
}

int wbc_file_decrypt_aes(char* inFile, char* outFile)
{
	char nBuf[FREAD_COUNT+BLOCK_SIZE];
	int nLen=0, nTotalSize=0, nSaveLen=0, nWLen=0;

	FILE *nRFp= fopen(inFile, "rb");
	if( nRFp == NULL ){
		fprintf(stderr,"[ERROR] %d can not fopen('%s')\n",__LINE__, inFile);
		return uFalse;
	}

	FILE *nWFp=fopen(outFile, "wb");
	if( nWFp == NULL ){
		fprintf(stderr,"[ERROR] %d can not fopen('%s')\n",__LINE__, outFile);
		return uFalse;
	}

	memset(iv,0,sizeof(iv)); // the same iv
	AES_set_decrypt_key(testkey, 256 ,&gKey);

	fseek(nRFp ,0 ,SEEK_END);
	nTotalSize = ftell(nRFp);
	fseek(nRFp ,0 ,SEEK_SET);
	printf("DEC TotalSize:%d\n", nTotalSize);

	while(nLen = fread(nBuf, RW_SIZE, FREAD_COUNT, nRFp) ){
		if( FREAD_COUNT == 0 ){
			break;
		}
		nSaveLen+=nLen;
		nWLen=nLen;

		AES_cbc_encrypt(nBuf, nBuf, nLen, &gKey, iv, AES_DECRYPT);
		if(nSaveLen == nTotalSize ){ // check last block
			nWLen = nLen - nBuf[nLen-1];
			printf("dec padding size %d\n", nBuf[nLen-1]);
		}

		fwrite(nBuf, RW_SIZE, nWLen, nWFp);
	}

	fclose(nWFp);
	fclose(nRFp);

	return uTrue;
}


int wbc_wallet_encrypt(char* filePath, char* outPath) 
{
	char nOutFile[MAX_PATH_BUF];
	memset(nOutFile, 0x00, MAX_PATH_BUF);
	snprintf(nOutFile, MAX_PATH_BUF,  "%s", outPath);
	wbc_file_encrypt_aes(filePath, nOutFile);

	char nDecFile[MAX_PATH_BUF];
	memset(nDecFile, 0x00, MAX_PATH_BUF);
	snprintf(nDecFile, MAX_PATH_BUF, "/home/woni/walletbackup/decryptaion.dat");
	wbc_file_decrypt_aes(nOutFile, nDecFile);
	return uTrue;
}

int wbc_send_backupSv(char* aesFilePath)
{ 
	/*--*
	 * RSync Command Proccess
	 * 
	 * Client->BackupServer
	 * 
	 * rsync -azv backupdir userid@ip:servicename
	 *
	 *--*/
	
	char	nCmd[MAX_CMD_BUF];
	int	nRes =0;
	memset(nCmd, 0x00, MAX_CMD_BUF);

	snprintf(nCmd, MAX_CMD_BUF, 
			"rsync -azv %s/ %s@%s:%s",
			gConfig.backupDir,	
			gConfig.remoteUser,
			gConfig.remoteIp,
			gConfig.remoteSvc);
	
	printf("CurCmd:[%s]\n", nCmd);
	nRes = system(nCmd);
	if(nRes == 0) {
		printf("[wbc_send_backupSv] cmd SUCC!\n");
	}else {
		printf("[wbc_send_backupSv] cmd FAIL!\n");
		return uFalse;
	}

	return uTrue;
}

void wbc_timer_event(int signo)
{
	time_t  nTime;
	struct tm* nTimeInfo;
	char    nDate[MAX_STR_BUF];
	char	nCurCmd[MAX_CMD_BUF];
	int	nRes =0, nCmdRes =0;

	time(&nTime);
	nTimeInfo= localtime(&nTime);
	memset(nDate, 0x00, MAX_STR_BUF);

	snprintf(nDate, MAX_STR_BUF,
			 "%04d%02d%02d_%02d%02d%02d",
			nTimeInfo->tm_year + 1900,
			nTimeInfo->tm_mon + 1,
			nTimeInfo->tm_mday,
			nTimeInfo->tm_hour,
			nTimeInfo->tm_min,
			nTimeInfo->tm_sec);

	printf("CurTimeEvent:%s\n", nDate);

	/*--
	 * [1] Dump Wallet
	 * [2] Dump Wallet Encrypt
	 * [3] RSync Command
	 *--*/

	/* DumpWallet */
	char nWalletPath[MAX_PATH_BUF];
	char nWalletAesPath[MAX_PATH_BUF];
	memset(nWalletPath, 0x00, MAX_PATH_BUF);
	memset(nWalletAesPath, 0x00, MAX_PATH_BUF);

	if(!wbc_dump_wallet(&gConfig, nWalletPath, nWalletAesPath)) {
		printf("wallet backup Fail\n");
		nCmdRes = ERR_DUMP_WALLET;
		goto error;
		
	}else {
		printf("wallet backup Succ!\n");
	}
	
	/* DumpWallet Encrypt */
	if(nWalletPath[0] != 0x00) {
		if(!wbc_file_encrypt_aes(nWalletPath, nWalletAesPath)) {
			printf("wallet Encryption Fail\n");
			nCmdRes = ERR_ENCRYPT_WALLET;
			goto error;
		}else {
			/*-----------------------------------------*
			 * Daemon Command Succ! Delete <.dat> file *
			 *-----------------------------------------*/
			printf("wallet Encryption Succ\n");
			memset(nCurCmd, 0x00, MAX_CMD_BUF);
			snprintf(nCurCmd, MAX_CMD_BUF, "rm -rf %s", nWalletPath);
			printf("CurCmd:%s\n", nCurCmd);
			nRes = system(nCurCmd);
		}
	}else {
		printf("wallet Path value check Please!\n");
		nCmdRes = ERR_WALLET_PATH_INVALID;
		goto error;
	}


	/* RSync Command */
	nRes = wbc_send_backupSv(nWalletAesPath);
	if(nRes == uFalse) {
		printf("wbc_send_backupSv() FAIL");
		nCmdRes = ERR_RSYNC_CMD;
		goto error;	
	}

	alarm(gTimePeriod);
	return;

error:
	WBC_FAIL_MSG(nCmdRes);	
	alarm(gTimePeriod);
	return;
}

void wbc_errmsg_print(int cmd)
{
	switch(cmd) {
		case ERR_CLIENT_CMD_PARAM_INVAILD:
			printf("ERR: Client Command Param Invalid\n");
			break;
		case ERR_CLIENT_ENV_HOME_INVALID:
			printf("ERR: Initialization WBC_HOME check Failed\n");
			break;
		case ERR_CLIENT_CONFIG_ROAD:
			printf("ERR: Client Config Road Fail\n");
			break;
		case ERR_DUMP_WALLET:
			printf("ERR: Bitcoin-cli Dump Wallet Fail\n");
			break;
		case ERR_ENCRYPT_WALLET:
			printf("ERR: AES256 Encryption Fail\n");
			break;
		case ERR_RSYNC_CMD:
			printf("ERR: RSync Command Fail\n");
			break;
		case ERR_DECRYPT_WALLET:
			printf("ERR: AES256 Decryption Fail\n");
			break;
		case ERR_WALLET_PATH_INVALID:
			printf("ERR: WalletBackUp Path Check!\n");
		      	break;	
		case ERR_PUBKEY_INCORRECT:
			printf("ERR: The PublicKey is incorrect!\n");
		       	break;	
		default:
			printf("ERR: undefine! \n");
			break;
	}
	return;
}

int wbc_exe_isvalid(int argc, char** argv)
{
	char *nPubKey;
	char *nInPath;
	char *nOutPath;
	int	nRes =0;

	if(strncmp(argv[1], "dec", 3) != 0) {
		return uFalse;
	}

	// if(strncmp(argv[1], "dec", 3) == 0) {
	
	nPubKey = argv[2];
	nInPath = argv[3];
	nOutPath = argv[4];

	if(nPubKey[0] == 0x00) { return uFalse; }	
	if(nInPath[0] == 0x00) { return uFalse; }
	if(nOutPath[0] == 0x00) { return uFalse; }

	printf("nPubKey[%s]\nnInPath[%s]\nnOutPath[%s]\n", 
			nPubKey, nInPath, nOutPath);

	// 1. PubKey Check
	if(strncmp(nPubKey, testkey, 9) == 0) {
		wbc_file_decrypt_aes(nInPath, nOutPath);
	}else {
		// 2nd Print
		nRes = ERR_PUBKEY_INCORRECT;
		WBC_FAIL_MSG(nRes);
		return uFalse;
	}

	return uTrue;
}

char outMessage[] = {
	" Decryption File\n"
	"(cmd) ./WBClient dec pubkey filepath othpath \n"
	"(ex)  ./WBClient dec test 20180626.dat 20180626_dec.dat\n"
};

int main(int argc, char **argv)
{
	int nRes=0;

	char test[MAX_PATH_BUF];
	memset(test, 0x00, MAX_PATH_BUF);

	if(argc > 1) {
		if(strncmp(argv[1], "help", 4) == 0){
			printf("%s", outMessage);			
			exit (0);
		}else{
			if(argc != 5) {
				printf("%s", outMessage);
				exit (0);
			}
			int nRes = wbc_exe_isvalid(argc, argv);
			if(nRes == uTrue) {
				return uTrue;
			}else {
				nRes = ERR_CLIENT_CMD_PARAM_INVAILD;
				WBC_FAIL_MSG(nRes);
				return 0;
			}
		}
	}else {
		/* export WBC_HOME=mainPath (ex) /home/woni/WBClient */
		char	*nEnv;
		nEnv = getenv("WBC_HOME");
		if(nEnv == NULL) {
			nRes = ERR_CLIENT_ENV_HOME_INVALID;
			WBC_FAIL_MSG(nRes);
			return 0;
		}
	}

	char nConfigPath[MAX_PATH_BUF];

	memset(&(gConfig), 0x00, sizeof(wbcConfig));
	memset(nConfigPath, 0x00, MAX_PATH_BUF);

	/* [1] ConfigRoad */
	snprintf(nConfigPath, MAX_PATH_BUF, "%s/%s", getenv("WBC_HOME"), "WBClient.config");
	printf("Config Path=%s\n", nConfigPath);
	wbc_road_config(nConfigPath, &gConfig); 

	printf("============================================================\n");
	printf(" 1. Config Read\n");
	printf("============================================================\n");
	printf("Category[%d]\nbackupDir[%s] peroid[%d]\n"
		"remoteIp[%s] remoteSvc[%s]\nremoteUser[%s] remotePort[%d]\n",
		gConfig.dCategory, gConfig.backupDir,
		gConfig.period, gConfig.remoteIp, gConfig.remoteSvc,
		gConfig.remoteUser, gConfig.remotePort);
	printf("============================================================\n");


	/* [2] SetTimer 
	 * 
	 * - [2]-1. dumpwallet
	 * - [2]-2. wallet Encrypt
	 * - [2]-3. Rsync Command
	 * 
	 * */
	struct	sigaction nAction;
	struct	itimerval nTimer;
	int	nTimerValue = gConfig.period;
	gTimePeriod = nTimerValue;

	memset(&nAction, 0x00, sizeof(sigaction));
	nAction.sa_handler = &wbc_timer_event;
	sigemptyset(&nAction.sa_mask);
	nAction.sa_flags = 0;

	sigaction(SIGALRM, &nAction, 0);
	alarm(nTimerValue);

	while(1){
		sleep(1000); // 20180912 CPU 100% Issue
	};
	
	return 0;
}
