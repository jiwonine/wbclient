# WBClient (Wallet Backup Client)

각 암호화폐별, 지갑을 주기적으로 암호화(AES256)하여 백업전용 서버로 전달하는 역할을 하는 프로그램
현재 Bitcoin, Qtum, Sia, Ethereum, ERC20 만 되어있는 상태. 추후 추가되는 데몬에 따라 업데이트 예정
(Release: 1.0 version - 2018.7.4)



## 1. Wallet Backup Flow

WBClient 에서 어떤 절차를 통해서 Backup Server 로 파일이 전달되는지 설명한다.

```sequence
# Example of a comment
Note left of WBClient: ICO Deamon 구동 중
Note left of WBClient: 1.DumpWallet 수행
Note left of WBClient: 2.DumpWallet 암호화(AES256)
WBClient->Backup Server: 3.RSync Send\n(20180704_114550_aes.dat)
Note right of WBClient: default Port:873,22
Note right of Backup Server: 4.SvcPath/Wallet(20180704_11455_aes.dat)\nFileReceive
Note over WBClient,Backup Server: Firewall Setting (Allow IP/Port)
```

- **공통사항**

Ubuntu 16.04 Server
rsync version 3.3.1 

- **Client**

Client 는,  암호화폐 데몬(ex. bitcoind) 구동 중인 곳에 존재한다. 
주기적으로  암호화폐 데몬(ex. bitcoind) 제공하는 명령어를 통해 Dumpwallet 수행한다.
받아온 해당 지갑 데이터의 파일 명을 변경 후, 파일을 암호화한다. (AES 256 사용)

[YYYYMMDDHHMM_aes.dat]  (ex) 년월시분_aes.dat 
해당 파일을 Server 쪽으로 전송한다.

- **Backup Server**

Server 는 각 암호화폐별 지갑이 백업되는 곳이다. 
rsync 서비스를 등록하여, 해당 디렉토리에 파일이 다운로드 되도록 설정한다.



## 2. 개발 언어 및 요구사항 분석

**개발언어**		C & Shell Script
**사용**			rsync, openssl 1.1.1.-pre8

**요구사항 분석**

- 데몬들의 Wallet 백업 방안 분석
- 데몬들을 통해 얻은 Wallet 데이터를 암호화 (AES 256)
- 데몬들의 Wallet 백업 진행하는 타이머 설정
- 백업서버에서 해당 Wallet 데이터 복호화해서 확인

**구현방법 정리**

- 데몬들의 Wallet 관련 command 검색 및 시험
- 암호화를 위한 OpenSSL 1.1.1-pre8 빌드 및 라이브러리 구성
- rsync src/dest 및 타이머 설정을 위한 프로그램 configure 구성
- 프로그램에서 암호화 / 복호화 모두 수행  



## 3. WBClient Process

```flow
st=>start: (1) WBClient 구동
op_config=>operation: (2) Road WBClient.config
op_setTimer=>operation: (3) SetTimer
sub_TimerEvent=>subroutine: (4) TimerEventHandler
op_dumpwallet=>operation: (5) DumpWallet
op_encrypt=>operation: (6) Encryption DumpWallet
op_rsync=>operation: (7) Rsync Command
io_succ=>inputoutput: (8) Signal Wait
cond=>condition: (9) Signal Alarm or Quit?
op_sigquit=>operation: (10) Signal Quit 
end=>end: (11) WBClient 구동종료

st->op_config->op_setTimer->sub_TimerEvent
sub_TimerEvent->op_dumpwallet->op_encrypt->op_rsync
op_rsync->io_succ
io_succ->cond
cond(yes, right)->sub_TimerEvent
cond(no)->op_sigquit
op_sigquit->end

```

(1) WBClient 구동

(2) Road WBClient.config
Category, BackupDir, RemoteUser, RemoteIp, RemoteSvc, RemotePort, RemotePeriod, MonPath

| 구분         | 속성    | 설명                                                         |
| ------------ | ------- | ------------------------------------------------------------ |
| Category     | integer | 해당 데몬분류 [1] BTC [2] ETH+ERC20 [3] QTUM [4] SIA         |
| BackupDir    | string  | 데몬 Wallet 백업할 디렉터리 지정 (ex) /home/woni/wbc/wallet_backup |
| RemoteUser   | string  | BackupServer Rsync uid 기입                                  |
| RemoteIp     | string  | BackupServer IP                                              |
| RemoteSvc    | string  | BackupServer 설정한 Service Name (실제 서비스명이 저장될 디렉터리) |
| RemotePort   | integer | BackupServer Port                                            |
| RemotePeriod | integer | Timer 설정 (초단위) (ex: 3600 - 1시간 주기적인 백업)         |
| MonPath      | string  | ETH, ERC20, SIA 의 경우 dumpWallet 기능이 없음. <br />이로 인해 데몬구동위치 지정 (optional) |

(3) SetTimer
위의 설정한 RemotePeriod 값 기반으로 Timer Event Handler 호출을 위한 설정을 진행한다.

(4) TimerEventHandler
타이머 설정 한 이후, SIG ALARM 발생 시 (5)~(7) 역할을 수행한다. 이후 대기 상태로 빠진다.

(5) DumpWallet
각 구동 중인 데몬의 Wallet 을 백업하는 역할을 수행한다.
현재 비트코인, QTUM 의 경우, bitcoin-cli 명령어로 수행되고 있다.
 Wallet Uplock 후  단계별로 진행되기에 Shell Script 로 수행한다.
이외 SIA, ETH/ERC20 의 경우, cli 명령어 중 dumpwallet 기능을 수행하는 부분이 없다. 
그래서 직접 CLI 에서 wallet unlock 사용없이 Wallet 데이터 조회가 가능하다.
이 부분을 파일 출력으로 저장하는 간단 수행으로 처리했다. (Shell Script 로 진행되는 부분은 Bitcoin/QTUM 만 해당)
정상적인 수행 결과로 " 년월일_시분초.dat " 파일이 생성된다. 

(6) Encryption DumpWallet
위의 (5) 수행하여 나온 결과인 파일을 " 년월일_시분초__aes.dat " 암호화하여 파일 생성한다.
OpenSSL 제공하는 해당 함수를 호출하여 Encryption 진행한다.

- int AES_set_encrypt_key(const unsigned char *userKey, const int bits,
                          AES_KEY *key);
- void AES_cbc_encrypt(const unsigned char *in, unsigned char *out,
                       size_t length, const AES_KEY *key,
                       unsigned char *ivec, const int enc);

 (7) Rsync Command
해당 Client 에서 rsync 명령어를 사용하여, BackupServer 쪽으로 (6) 에서 생성된 파일을 전송한다.



## 4. WBClient 구동을 위한 설정

### 4-1. rsync  setting

`vi /etc/rsyncd.conf`

> [woni-test] 
>
> path=/home/woni 
>
> uid=woni // 사용자ID 
>
> gid=woni // 사용자의 그룹ID 
>
> host allow=Client IP 
>
> write only=yes 

`rsync --daemon`

`netstat -pant | grep rsync`

해당 데몬이 구동되었는지 확인한다. 별도의 Port 설정이 없었다면, 873 포트 LISTEN 상태를 확인할 수 있다.
각 서비스 별로 (BTC/ETH/ECR20/QTUM...) 설정이 필요하다. 



### 4-2. SSH Public Key setting

위의 rsync 명령어 수행 시, 패스워드를 입력해야한다. 
이 절차를 생략하고자 하는 경우, ssh Public Key 를 BackupSever 쪽에 등록해준다.

#### (1) Client - ssh keygen 생성

```
woni@woni-VM2:~$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/woni/.ssh/id_rsa):
/home/woni/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/woni/.ssh/id_rsa.
Your public key has been saved in /home/woni/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:lm+mtmltzz8geL7ON6wVD04Zc7bjbWtvIhStt8RDpzw woni@woni-VM2
The key's randomart image is:
+---[RSA 2048]----+
| |
| |
| o.o |
| . .*o..|
| S. =*oo |
| ...oo+=Eo |
| +++o+o+o|
| o*+o= +oo|
| o++==o+o=o|
+----[SHA256]-----+
```



#### (2) BackupServer - Send SSH Public Key

-  Server 에 직접 옮겨서 authorized_key 에 등록하는 방법 

`cat id_rsa.pub >> ~/.ssh/authorized_keys `

- ssh-copy-id 명령어 사용 

  `ssh-copy-id -i id_rsa.pub -p 포트번호 uid@BackupServerIP` 

  

### 4-3. WBClient setting

#### (1) 환경설정

`export LD_LIBRARY_PATH=/usr/local/lib`
`export WBC_HOME=/home/사용자id/wbc`

이외 필요한 환경변수 설정을 추가해주면된다. ~/.bashrc 에 등록해준다.
프로그램 설치 시, library 경로에 'libcrypto.so.1.1' 복사한다. 권한문제가 있을 경우, WBC_HOME 경로로 지정해도 된다.

#### (2) WBClient 구성 파일

> ![](E:\0000_정리하기\002 개발관련\001_개발내역정리_02.png)   총 4개의 파일

- WBClient - 실제 구동되는 프로그램

- WBClient.config - 구동 시, 해당설정 값들을 토대로 구성

- WBCRun.sh - Bitcoin , Qtum 구동 시 필요한 Shell Script

- libcrypto.so.1.1 - 암호화/복호화를 위해 사용되는 라이브러리 

   

## 5. WBClient  시험

## 6. WBClient 이슈사항



