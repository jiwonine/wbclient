#!/bin/sh

backupDir="/home/woni/walletbackup"
monInputCmd=$2
monPath=
monEnv=$PASSPHRASE
monCmdresult=
monFirstCmd=
monSecondCmd=

if [ ! $WBC_HOME ]; then
	echo "Initialization WBC_HOME check Fail"
	echo
	exit 0
fi

dump_cmd_Bitcoin()
{
	## (ex)
	## bitcoin-cli -regtest -rpcuser=woni -rpcpassword=wonitest dumpwallet 
	if [ -z "$monInputCmd" ]; then
		echo "Error filePath"
	else
		monPath="/home/woni/bitcoin-cli"
		monFirstCmd=$monPath' walletpassphrase '$monEnv' 60'
		monCmdresult=`$monFirstCmd`
		
		monSecondCmd=$monPath' dumpwallet '$monInputCmd
		monCmdresult=`$monSecondCmd`
		echo "$monCmdresult"
		## Error Print
	fi
	
}

dump_cmd_Qtum()
{
	## (ex)
	## qtum-cli -regtest -rpcuser=woni -rpcpassword=wonitest dumpwallet
	if [ -z "$monInputCmd" ]; then
		echo "Error filePath"
	else
		monPath="/home/woni/qtum-cli"
                monFirstCmd=$monPath' walletpassphrase '$monEnv' 60'
                monCmdresult=`$monFirstCmd`

                monSecondCmd=$monPath' dumpwallet '$monInputCmd
                monCmdresult=`$monSecondCmd`
                echo "$monCmdresult"
                ## Error Print
		## (ex) error: Authorization failed: Incorrect rpcuser or rpcpassword
	fi

}

dump_cmd_Ion()
{
        ## (ex)
        ## ion-cli -regtest -rpcuser=woni -rpcpassword=wonitest dumpwallet
        if [ -z "$monInputCmd" ]; then
                echo "Error filePath"
        else
                monPath="/home/woni/ion-cli"
                monFirstCmd=$monPath' walletpassphrase '$monEnv' 60'
                monCmdresult=`$monFirstCmd`

                monSecondCmd=$monPath' dumpwallet '$monInputCmd
                monCmdresult=`$monSecondCmd`
                echo "$monCmdresult"
                ## Error Print
                ## (ex) error: Authorization failed: Incorrect rpcuser or rpcpassword
        fi

}

dump_cmd_Ripple()
{
	echo "Ripple"
}

dump_cmd_Sia()
{
	## (ex)
	## siac wallet addresses >> test.dat
	if [ -z "$monInputCmd" ]; then
		echo "Error filePath"
	else	
		monPath="/home/woni/siac"
		monFirstCmd=$monPath' wallet addresses >> '$monInputCmd
		monCmdResult=`$monCurCmd`
		echo "$monCmdResult"
	fi
}


dump_cmd_Ethereum()
{
	## (ex)
	## geth account list >& test.dat
	if [ -z "$monInputCmd" ]; then
		echo "Error filePath"
	else
		monPath="/home/jwlim/ICO/Eth/geth --datadir=/home/jwlim/test-data/eth"
		monFirstCmd=$monPath' account list' 
		echo "subShellCmd=$monFirstCmd >> $monInputCmd"
		exec $monFirstCmd >> $monInputCmd
		if [ "monFirstCmd" == 0 ] ;then
		 echo "cmd success" >&2
		else
		 ret=$?
		 echo "cmd fail $ret" >&2
		 exit $ret
		fi
		exit 0
		echo "Succ"
	fi
}

usage()
{
	echo " USAGE : WBCRun.sh [dumpBTC | dumpRIP | dumpQTUM | dumpION | dumpSIA | dumpETH]"
	echo
}



case "$1" in
	"dumpBTC" )
		dump_cmd_Bitcoin
		;;
	"dumpRIP" )
		dump_cmd_Ripple
		;;
	"dumpQTUM" )
		dump_cmd_Qtum
		;;
	"dumpION" )
		dump_cmd_Ion
		;;
	"dumpSIA" )
		dump_cmd_Sia
		;;
	"dumpETH" )
		dump_cmd_Ethereum
		;;
	*)
		usage
		exit 0
		;;
esac

